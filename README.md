> Warning: This is a pre-release version, we are actively developing this repository. Issues, bugs and features will happen, rise and change. WE DO NOT RECOMMAND USING THE CURRENT RELEASE!

# CAI Reported

We foster the openness, integrity, and reproducibility of scientific research.

Scripts and tools used to develop a pipeline to analyse CAI adaptations.


## How to use this repository?

This repository host both the scripts and tools developed by this study. Feel free to adapt the scripts and tools, but remember to cite their authors!

To look at our scripts and raw results, **browse** through this repository. If you want to reproduce our results you will need to **clone** this repository, build the docker, and the run all the scripts. If you want to use our data for our own research, **fork** this repository and **cite** the authors.


## Prepare a docker

All required files and tools run in a self-contained [docker](https://www.docker.com/) image.

#### Clone the repository

```
git clone https://github.com/pseudogene/cai-reported.git
cd cai-reported
```

#### Create a docker

```
docker build --rm=true -t cai-reported .
```

#### Start the docker

To import and export the results of your analysis, you need to link a folder to the docker. In this example your data will be store in `results` (current filesystem) which will be seem as been `/data` from within the docker by using `-v <USERFOLDER>:/data`.

```
docker run -i -t --rm -v $(pwd)/results:/data cai-reported /bin/bash
```

## Importation new reference genomes
Download a new genome from NCBI Genbank or Ensembl and uncompress it

```
wget ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF_000233375.1_ICSASG_v2/GCF_000233375.1_ICSASG_v2_cds_from_genomic.fna.gz
gunzip GCF_000233375.1_ICSASG_v2_cds_from_genomic.fna.gz
```

Convert to CDS sequences into a codon usage table

```
cusp -sequence GCF_000233375.1_ICSASG_v2_cds_from_genomic.fna -outfile Salmo_salar_cds.cusp  
codcopy -infile Salmo_salar_cds.cusp -oformat2 cutg -outfile Salmo_salar_cds.cutg
mv Salmo_salar_cds.cutg genomes/
```

## Importation new genome annotations
Download a new genome annoation from NCBI Genbank as GenBank format

```
seqret -sequence GenBank::new_genome.gb -outseq embl::new_genome.embl -feature
cp new_genome.embl embl/
```
or

Download a new genome annotation from EBI as EMBL format

```
cp new_genome.embl embl/
```

## Import sequences to annotate

Copy all your FASTA formated genome in the same folder.

```
./annotate_fasta.pl --fasta <fasta_folder> -model <embl_folder>
```


## Run a new analysis

Copy all your EMBL (.embl) or GENBANK (.gb) formated genome in the same folder. Rename your files as `<year-as-YYYY>_<sample-name>_.embl/gb`.

```
./cai_pipeline.pl --in <embl/gb_folder> --embl --genomes <genomes folder> -attrib region --strict
```