#!/usr/bin/perl
# $Revision: 1.3 $
# $Date: 2018/03/16 $
# $Id: cai_pipeline.pl $
use strict;
use warnings;
use Getopt::Long;
use Cwd qw/ abs_path getcwd /;
use File::Temp qw/ tempdir tempfile /;
use Bio::SeqIO;
use Bio::Seq;
use Bio::AlignIO;
use Bio::Factory::EMBOSS;

#----------------------------------------
our $VERSION = 1.3;

#----------------------------------------
sub _getorf
{
    my $seq     = shift;
    my $factory = new Bio::Factory::EMBOSS;
    my $seqio;
    my $app = $factory->program('getorf');
    my ($OUT, $filename) = tempfile(DIR => '/tmp', UNLINK => 1);
    print {$OUT} ">seq\n", $seq->seq(), "\n";
    close $OUT;
    my %input = (-sequence => $filename, -minsize => 50, -reverse => q{N}, -methionine => q{N}, -outseq => $filename . '.out');
    $app->run(\%input);
    eval { $seqio = Bio::SeqIO->new(-file => $filename . '.out'); };

    unless ($@)
    {
        my $max = 0;
        my $tmp;
        while (my $seqobj = $seqio->next_seq())
        {
            if ($seqobj->length() > $max)
            {
                $tmp = $seqobj;
                $max = $seqobj->length();
            }
        }
        unlink($filename);
        unlink($filename . '.out');
        my ($start, $stop) = ($1, $2) if ($tmp->description =~ m/^\[(\d+) - (\d+)\]/g);
        return ($start, $stop, $max);
    }
    return;
}
my ($verbose, $threads, $outdir, $alt, $strict, $embl, $genbank, $attrib, $seqdir, $align, $genome, $genomedir, $white) = (0, 1, abs_path('./output'), 0, 0, 0, 0, 'regions');
GetOptions(
           'attrib:s'    => \$attrib,
           'strict:+'    => \$strict,
           'align:s'     => \$align,
           'in:s'        => \$seqdir,
           'embl+'       => \$embl,
           'genbank+'    => \$genbank,
           'alt+'        => \$alt,
           'g|genomes:s' => \$genomedir,
           'w|white:s'   => \$white,
           'o|output:s'  => \$outdir,
           't|threads:+' => \$threads,
           'v|verbose+'  => \$verbose
          );
if (   ((defined $seqdir && -d abs_path($seqdir) && defined $attrib && $attrib =~ m/^(cds|mat_peptide|region)$/) or (defined $align && -d abs_path($align)))
    && $outdir
    && (-d abs_path($outdir) || !-e abs_path($outdir))
    && defined $threads
    && int($threads) > 0)
{
    mkdir abs_path($outdir) if (!-e abs_path($outdir));
    $outdir  = abs_path($outdir);
    $seqdir  = abs_path($seqdir);
    $embl    = 1 if ($embl == 0 && $genbank == 0);
    $genbank = 0 if ($embl != 1);
    my %whitelist;
    if (defined $white && -r abs_path($white) && open(my $in, q{<}, abs_path($white)))
    {
        print {*STDERR} ">Reading white list...\n" if ($verbose);
        while (<$in>)
        {
            next if (m/^#/ || length($_) <= 3);
            chomp;
            $whitelist{$_} = 1;
        }
        close $in;
    }
    my %taglength;
    if (!defined $align && defined $seqdir && -d abs_path($seqdir) && defined $attrib && $attrib =~ m/^(cds|mat_peptide|region)$/ && opendir(my $dh, $seqdir))
    {
        print {*STDERR} ">Extract sequences...\n" if ($verbose);
        my $type = ($embl ? 'embl' : ($genbank ? 'gb' : 'seq'));
        while (readdir $dh)
        {
            if (m/^(.*)\.$type$/)
            {
                print {*STDERR} '  * Extracting from ', $_, "\n" if ($verbose);
                my $seqio = Bio::SeqIO->new('-format' => ($embl ? 'embl' : ($genbank ? 'genbank' : 'auto')), '-file' => $seqdir . q{/} . $_);
                while (my $seq = $seqio->next_seq)
                {
                    for my $feat_object ($seq->get_SeqFeatures)
                    {
                        if (uc $feat_object->primary_tag eq uc $attrib)
                        {
                            my $name;
                            for my $tag ($feat_object->get_all_tags)
                            {
                                if ($tag eq 'locus_tag' or $tag eq 'region_name' or $tag eq 'product')
                                {
                                    for my $value ($feat_object->get_tag_values($tag)) { $name = $value; }
                                }
                            }
                            if (defined $name && (!%whitelist || exists $whitelist{$name}) && open(my $out, '>>', $outdir . q{/} . $name . '.fasta'))
                            {
                                if ($strict)
                                {
                                    my ($start, $stop, $length) = _getorf($feat_object->seq);
                                    if (defined $length)
                                    {
                                        $taglength{$name} = $stop - $start + 1 if (!exists $taglength{$name} || $taglength{$name} < $stop - $start + 1);
                                        print {$out} q{>}, $1, "\n", $feat_object->seq->subseq($start, $stop), "\n";
                                    }
                                    if ($feat_object->seq->length() % 3 != 0) { print {*STDOUT} "WARNING\t $1 TAG $name is not divided by three.\n"; }
                                }
                                else
                                {
                                    if ($feat_object->seq->length() % 3 == 0)
                                    {
                                        $taglength{$name} = $feat_object->seq->length() if (!exists $taglength{$name} || $taglength{$name} < $feat_object->seq->length());
                                        print {$out} q{>}, $1, "\n", $feat_object->seq->seq(), "\n";
                                    }
                                    else { print {*STDOUT} "ERROR\t $1 TAG $name is not divided by three.\n"; }
                                }
                                close $out;
                            }
                        }
                    }
                }
            }
        }
        closedir $dh;
        if (opendir(my $dh, $outdir))
        {
            print {*STDERR} ">Align sequences\n" if ($verbose);
            while (readdir $dh)
            {
                if (m/^(.*)\.fasta$/)
                {
                    print {*STDERR} '  * Aligning ', $_, "\n" if ($verbose);
                    if ($alt)
                    {
                        system('GramAlign -q -F 1 -f 2 -i ' . $outdir . q{/} . $_ . ' -o ' . $outdir . q{/} . $1 . '.align');
                        unlink '_ga_temp.page0' if (-e '_ga_temp.page0');
                    }
                    else { system('clustalo' . (int($threads) > 1 ? ' --thread ' . int($threads) : q{}) . ' -i ' . $outdir . q{/} . $_ . ' --infmt fasta -o ' . $outdir . q{/} . $1 . '.align --outfmt fasta'); }
                    if (-r $outdir . q{/} . $1 . '.align')
                    {
                        my $in = Bio::AlignIO->new(-format => 'fasta', -file => $outdir . q{/} . $1 . '.align');
                        while (my $aln = $in->next_aln()) { $taglength{$1} = $aln->length, print {*STDOUT} "WARRING\t $1 SEQUENCES are not flush.\n" if (!$aln->is_flush()); }
                        unlink $outdir . q{/} . $_;
                        system('mv ' . $outdir . q{/} . $1 . '.align' . q{ } . $outdir . q{/} . $_);
                    }
                }
            }
            closedir $dh;
            $align = $outdir;
        }
    }
    if (defined $align && -d abs_path($align) && defined $genomedir && -d abs_path($genomedir) && opendir(my $dh, abs_path($genomedir)) && open(my $log, q{>}, $outdir . '/cai.tsv'))
    {
        print {$log} "genome\tgene\tsample\tyear\tlength\tecai\tall_sample_cai\tcai\tcai_threshold\n";
        $genomedir = abs_path($genomedir);
        while (readdir $dh)
        {
            if (m/^(.*)\.cutg$/)
            {
                print {*STDERR} ">Calculate CAI for $1\n" if ($verbose);
                my (%eCAI, %CAI);
                $genome = $1;
                mkdir $outdir . q{/} . $genome if (!-e $outdir . q{/} . $genome);
                if (opendir(my $dh2, $outdir))
                {
                    while (readdir $dh2)
                    {
                        if (m/^(.*)\.fasta$/)
                        {
                            my $name = $1;
                            print {*STDERR} '  * Accessing ', $name, "\n" if ($verbose);
                            system(  'perl /usr/local/bin/CAIcal_ECAI_v1.4.pl -e cai_and_expected -f '
                                   . $outdir . q{/}
                                   . $_ . ' -h '
                                   . $genomedir . q{/}
                                   . $genome
                                   . '.cutg -o3 '
                                   . $outdir . q{/}
                                   . $genome . q{/}
                                   . $name . '_vs_'
                                   . $genome
                                   . '.out -o1 '
                                   . $outdir . q{/}
                                   . $genome . q{/}
                                   . $name . '_vs_'
                                   . $genome
                                   . '.tsv -l '
                                   . $taglength{$name}
                                   . ($verbose ? q{} : ' >/dev/null 2>/dev/null'));
                            if (-r $outdir . q{/} . $genome . q{/} . $name . '_vs_' . $genome . '.out' && -r $outdir . q{/} . $genome . q{/} . $name . '_vs_' . $genome . '.tsv')
                            {
                                if (open my $in, q{<}, $outdir . q{/} . $genome . q{/} . $name . '_vs_' . $genome . '.out')
                                {
                                    while (<$in>)
                                    {
                                        if (m/Tolerance Limit \(eCAI\):\s+(-?\d+\.\d+)/) { $eCAI{$name} = $1 }
                                        elsif (m/CAI Average:\s+(-?\d+\.\d+)/) { $CAI{$name} = $1 }
                                    }
                                    close $in;
                                }
                                if (open my $in, q{<}, $outdir . q{/} . $genome . q{/} . $name . '_vs_' . $genome . '.tsv')
                                {
                                    <$in>;
                                    while (<$in>)
                                    {
                                        chomp;
                                        my @data = split m/\t/;
                                        if (scalar @data == 2)
                                        {
                                            my $year;
                                            if ($data[0] =~ m/\D((20|19)\d{2})\D/) { $year = $1; }
                                            if (exists $eCAI{$name} && exists $CAI{$name})
                                            {
                                                print {$log} $genome, "\t", $name, "\t", substr($data[0], 1), "\t", (defined $year ? $year : q{}), "\t", $taglength{$name}, "\t", $eCAI{$name}, "\t", $CAI{$name}, "\t", $data[1], "\t",
                                                  ($data[1] / $eCAI{$name}), "\n";
                                            }
                                        }
                                    }
                                    close $in;
                                }
                            }
                            else { print {*STDERR} "FAILURE\t $name was not successfull at calculating CAI\n"; }
                        }
                    }
                    closedir $dh2;
                }
                if (%eCAI && %CAI)
                {
                    foreach my $item (sort keys %eCAI)
                    {
                        if (exists $CAI{$item}) { print {*STDOUT} $genome, "\t", $item, "\t", $eCAI{$item}, "\t", $CAI{$item}, "\t", ($CAI{$item} / $eCAI{$item}), "\n"; }
                    }
                }
            }
        }
        closedir $dh;
    }
}
else
{
    print {*STDOUT} "Usage: $0 --in <folder_with_embl/genbank_file> --genomes <reference_models>\n\nexample: $0 --in ipnv --embl --attrib region --genomes genomes --strict\n\n";
    exit 1;
}
