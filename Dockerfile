FROM ubuntu:17.10

ENV RATT_HOME /usr/local/bin
USER root

RUN apt-get update

RUN DEBIAN_FRONTEND=noninteractive apt-get install -y emboss emboss-lib clustalo --no-install-recommends

RUN DEBIAN_FRONTEND=noninteractive apt-get install -y wget ca-certificates --no-install-recommends && \
    wget -q ftp://ftp.umiacs.umd.edu/pub/hiren/Hiren/HiC_Samples/Scripts/BioJava/CAIcal_ECAI_v1.4_source/CAIcal_ECAI_v1.4.pl && \
    chmod 755 CAIcal_ECAI_v1.4.pl && \
    mv CAIcal_ECAI_v1.4.pl /usr/local/bin/

RUN DEBIAN_FRONTEND=noninteractive apt-get install -y bioperl bioperl-run --no-install-recommends

RUN DEBIAN_FRONTEND=noninteractive apt-get install -y mummer subversion --no-install-recommends && \
    svn co "https://svn.code.sf.net/p/ratt/code/" ratt-code && \
    rm -rf ratt-code/.svn && \
    chmod 755 ratt-code/main.ratt.pl && \
    sed -i -e 's,defined(@{$$ref_shift{$refName}}),exists $$ref_shift{$refName},g' ratt-code/main.ratt.pl && \
    cp ratt-code/* /usr/local/bin/ && \
    rm -rf ratt-code

#RUN DEBIAN_FRONTEND=noninteractive apt-get install -y build-essential unzip --no-install-recommends
#RUN wget http://bioinfo.unl.edu/downloads/GramAlign3_00.zip -O GramAlign3_00.zip && \
#    unzip GramAlign3_00.zip && \
#    make -C GramAlign3_00/src -k clean && \
#    sed -i 's/CFLAGS = -O3 -funroll/CFLAGS = -O2 -funroll/' GramAlign3_00/src/Makefile && \
#    make -C GramAlign3_00/src && \
#    install GramAlign3_00/src/GramAlign /usr/local/bin/ && \
#    rm -rf  GramAlign3_00  GramAlign3_00.zip  __MACOSX

COPY *.pl /usr/local/bin/

RUN mkdir /data
WORKDIR /data
