#!/usr/bin/perl
# $Revision: 1.3 $
# $Date: 2018/03/16 $
# $Id: annotate_fasta.pl $
use strict;
use warnings;
use Getopt::Long;
use Cwd qw/ abs_path getcwd /;
use File::Temp qw/ tempdir tempfile /;
use Bio::SeqIO;
use Bio::Seq;
use Bio::AlignIO;
use Bio::Factory::EMBOSS;

#----------------------------------------
our $VERSION = 1.3;

#----------------------------------------
my ($verbose, $threads, $outdir, $seqdir, $annodir) = (0, 1, abs_path('./output'));
GetOptions('f|fasta=s' => \$seqdir, 'm|model=s' => \$annodir, 'o|output:s' => \$outdir, 't|threads:+' => \$threads, 'v|verbose+' => \$verbose);
if (defined $seqdir && -d abs_path($seqdir) && defined $annodir && -d abs_path($annodir) && $outdir && (-d abs_path($outdir) || !-e abs_path($outdir)) && defined $threads && int($threads) > 0)
{
    mkdir abs_path($outdir) if (!-e abs_path($outdir));
    $outdir  = abs_path($outdir);
    $seqdir  = abs_path($seqdir);
    $annodir = abs_path($annodir);
    mkdir $outdir . '/genomes' if (!-e $outdir . '/genomes');
    print {*STDERR} ">Transferring annotations...\n" if ($verbose);
    if (opendir(my $dh, $seqdir))
    {
        my $curdir = getcwd;
        while (readdir $dh)
        {
            my $file = $_;
            if ($file =~ m/^(.*)\.(fa|fsa|fasta)$/)
            {
                my $name = $1;
                my ($newyear, $newname);
                print {*STDERR} '  * Annotating ', $file, "\n" if ($verbose);
                my $dir = tempdir(CLEANUP => 1);
                my $seqfasta = Bio::SeqIO->new(-file => $seqdir . q{/} . $file, -format => 'fasta');
                my $seq = $seqfasta->next_seq;
                $newyear = $1 if ($seq->desc() =~ m/\[year=(\d+)\]/g);
                $newname = $1 if ($seq->desc() =~ m/\[sample=([^\] ]+)\]/g);
                $seqfasta->close();
                chdir $dir;
                system('start.ratt.sh ' . $annodir . q{ } . $seqdir . q{/} . $file . q{ } . $name . ' Species >/dev/null 2>/dev/null');

                if (-r abs_path($dir . q{/} . $name . q{.} . $name . '.final.embl'))
                {
                    my $seqin = Bio::SeqIO->new(-file => $dir . q{/} . $name . q{.} . $name . '.final.embl', -format => 'embl');
                    my $seqout = Bio::SeqIO->new(-file => q{>} . $outdir . q{/} . (defined $newyear && defined $newname ? $newyear . q{_} . $newname : (defined $newname ? $newname : $name)) . '.embl', -format => 'embl');
                    $seq = $seqin->next_seq;
                    $seq->display_id($newname) if (defined $newname);
                    $seq->add_date($newyear)   if (defined $newyear);
                    $seq->accession_number($name);
                    $seqout->write_seq($seq);
                    $seqout->close();
                    $seqin->close();
                }
                chdir $curdir;
                system('rm -rf ' . $dir);
            }
        }
        chdir $curdir;
        closedir $dh;
    }
}
else
{
    print {*STDOUT} "Usage: $0 --fasta <fasta_file> --model <folder_with_embl_reference>\n\nexample: $0 --fasta ipnv --model embl -v\n\n";
    exit 1;
}
